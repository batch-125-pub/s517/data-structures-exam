let collection = [];

// Write the queue functions below.

function print(){

	return collection;
}

function enqueue(x){

	console.log("x", x);
	if(collection.length == 0){
		collection[0] = x;
	} else {
		collection[collection.length] = x;
	}

	console.log("collection:", collection);
	
	return collection;

}

function dequeue(){
	console.log("before dequeue: " , collection)
	for(let i = 0; i < collection.length; i++){
		collection[i] = collection[i+1];
	}

	tempCol = [];

	tempCol = collection.slice(0, collection.length-1);

	collection = tempCol;

	console.log("after dequeue", collection);
	return collection;
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	console.log("currect col", collection);
	if(collection.length != 0){
		return false;
	} else {
		return true;
	}
}
module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};